package com.addcel.prosa.dto;

public class Monto {

	// [{"idMonto":3,"monto":50},{"idMonto":4,"monto":100},{"idMonto":5,"monto":200},{"idMonto":6,"monto":500},{"idMonto":7,"monto":1000}]
	
	private int idMonto;
	private double monto;
	
	public int getIdMonto() {
		return idMonto;
	}
	public void setIdMonto(int idMonto) {
		this.idMonto = idMonto;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	
	public String toString(){
		return Double.toString(monto);
	}
}
