package com.addcel.prosa.dto;

public class Token {

	//{"token":"lbvKI4Zd3uxPOboykBPt+aQYaZUGvC3D6MV1iIDRpo0=","idError":0,"mensajeError":""}
	
	private String token;
	private int idError;
	private String mensajeError;
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	
	
}
