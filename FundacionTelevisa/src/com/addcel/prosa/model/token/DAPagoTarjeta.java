package com.addcel.prosa.model.token;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.connection.http.MethodGET;
import com.addcel.api.util.UtilBB;
import com.addcel.prosa.dto.PagoRespuesta;
import com.addcel.prosa.dto.Token;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.view.base.Viewable;

public class DAPagoTarjeta extends DataAccessible implements Runnable {

	
	private JSONObject jsonObject;
	private String monto;
	
	public DAPagoTarjeta(Viewable viewable, String data, String monto) {
		super(viewable, data);
		this.monto = monto;
	}

	
	public void setJson(JSONObject jsonObject){
		
		this.jsonObject = jsonObject;
	}
	
	public void run() {
		execute(viewable, data);
	}

	public void execute(Viewable viewable, String data) {

		try {

			this.encryptionable = new EncryptToken();
			encryptionable.execute(data);
			String encrypt = (String) encryptionable.getData();
			
			connectable = new MethodGET(URL.URL_GET_TOKEN);
			connectable.execute(encrypt);
			
			String descrypt = (String)connectable.getData();
			

			this.descryptionable = new DescryptToken();
			
			descryptionable.execute(descrypt);
			
			descrypt = (String)descryptionable.getData();
			
			
			this.objectable = new OToken();
			objectable.execute(descrypt);
			Token token = (Token)objectable.getData();
			
			
			if (token.getIdError() > 0){
				
				viewable.setData(DataAccessible.ERROR, token.getMensajeError());
			} else {

				try {
					//JSONObject jsonObject = new JSONObject(json);
					jsonObject.put("token", token.getToken());
					jsonObject.put("imei", UtilBB.getImei());
					jsonObject.put("modelo", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
					jsonObject.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
					
					//EncryptToken encryptToken = new EncryptToken();
					encryptionable = new EncryptSensitive();
					encryptionable.execute(jsonObject.toString());
					encrypt = (String)encryptionable.getData(); 
					
					connectable = new MethodGET(URL.URL_GET_PAGOS);
					connectable.execute(encrypt);
					String info = (String)connectable.getData();
					
					
					descryptionable = new DescryptSensitive();
					descryptionable.execute(info);
					String info2 = (String)descryptionable.getData();
					
					OPagoRespuesta oPagoRespuesta = new OPagoRespuesta();
					oPagoRespuesta.execute(info2);
					PagoRespuesta pagoRespuesta = (PagoRespuesta)oPagoRespuesta.getData();
					pagoRespuesta.setMonto(monto);
					
					viewable.setData(DataAccessible.DATA, pagoRespuesta);
					
				} catch (JSONException e) {
					e.printStackTrace();
					throw new OwnException(Error.JSON_EXCEPTION);
				}
			}

		} catch (OwnException e) {
			viewable.setData(DataAccessible.ERROR, e.toString());
		}
	}
}
