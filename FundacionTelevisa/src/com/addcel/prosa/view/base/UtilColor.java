package com.addcel.prosa.view.base;

import net.rim.device.api.ui.Color;

public class UtilColor {
	/*
	private final static int GREEN_01 = 0x90c1c7;
	private final static int GREEN_02 = 0x3db7b0;

	private final static int BLUE = 0x23486d;

	private final static int WHITE = Color.WHITE;
	private final static int RED = 0xc30042;
	
	private final static int GREY_01 = 0xececec;
	private final static int GREY_02 = 0xdfdfdf;
	private final static int GREY_03 = 0xb4bdb6;
	private final static int GREY_04 = 0x939598;
	private final static int GREY_05 = 0x231f20;
	*/
	
	private final static int BLANCO = Color.WHITE;
	private final static int GRIS = 0x636463;
	private final static int AMARILLO = 0xf6b454;
	private final static int NARANJA = 0xeb7a41;
	private final static int AZUL = 0x1e4367;
	
	
	public final static int MAIN_BACKGROUND = AZUL;

	public final static int TITLE_STRING = BLANCO;
	public final static int TITLE_BACKGROUND = NARANJA;

	public final static int BUTTON_FOCUS = AMARILLO;
	public final static int BUTTON_SELECTED = NARANJA;
	public final static int BUTTON_UNSELECTED = NARANJA;

	public final static int BUTTON_STRING_FOCUS = BLANCO;
	public final static int BUTTON_STRING_SELECTED = BLANCO;
	public final static int BUTTON_STRING_UNSELECTED = BLANCO;

	public final static int SUBTITLE_STRING = BLANCO;
	public final static int SUBTITLE_BACKGROUND = NARANJA;

	
	public final static int ELEMENT_STRING_CHOICE = BLANCO;
	public final static int ELEMENT_STRING = AMARILLO;
	public final static int ELEMENT_BACKGROUND = AZUL;

	public final static int EDIT_TEXT_DATA_FOCUS = GRIS;
	public final static int EDIT_TEXT_DATA_UNFOCUS = GRIS;
	
	public final static int EDIT_TEXT_BACKGROUND_FOCUS = BLANCO;
	public final static int EDIT_TEXT_BACKGROUND_UNFOCUS = BLANCO;

	public final static int LIST_DESCRIPTION_TITLE = NARANJA;
	public final static int LIST_DESCRIPTION_DATA = AMARILLO;

	public final static int LIST_BACKGROUND_SELECTED = BLANCO;
	public final static int LIST_BACKGROUND_UNSELECTED = AMARILLO;


	public static int toRGB(int r, int g, int b){
		return (r<<16)+(g<<8)+b;		 
	}

	public static int getBlack(){
		return toRGB(30, 30, 30);
	}

	public static int getDarkGray(){
		return toRGB(120, 120, 120);
	}

	public static int getLightGray(){
		return toRGB(200, 200, 200);
	}

	public static int getLightOrange(){
		return toRGB(255, 184, 101);
	}

	public static int getDarkOrange(){
		return toRGB(255, 159, 45);
	}
}
