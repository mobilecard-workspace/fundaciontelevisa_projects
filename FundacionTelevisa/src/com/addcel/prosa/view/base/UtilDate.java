package com.addcel.prosa.view.base;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class UtilDate {

	
	String meses[] = {"enero", "febrero", "marzo", 
			  "abril", "mayo", "junio", "julio", 
			  "agosto", "septiembre", "octubre", 
			  "noviembre", "diciembre"};
	
	
	public static String[] getMonths(){
		String meses[] = {"enero", "febrero", "marzo", 
				  "abril", "mayo", "junio", "julio", 
				  "agosto", "septiembre", "octubre", 
				  "noviembre", "diciembre"};
		return meses;
	}
	
	
	
	public static String[] getNumberMonths(){
		String meses[] = {"01", "02", "03", 
				  "04", "05", "06", "07", 
				  "09", "09", "10", 
				  "11", "12"};
		return meses;
	}
	
	
	public static String[] getYears(int value){
		
		String[] years = null;
		Vector vector = new Vector();
		
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(new Date());
		
		int year = calendar.get(calendar.YEAR);
		
		if (value < 0){
			for (int index = year; index > (year + value); index--){
				
				vector.addElement(String.valueOf(index));
			}
		} else {
			for (int index = year; index < (year + value); index++){
				
				vector.addElement(String.valueOf(index));
			}
		}
		
		years = new String[vector.size()];
		
		vector.copyInto(years);
		
		return years;
	}
	
	
}
