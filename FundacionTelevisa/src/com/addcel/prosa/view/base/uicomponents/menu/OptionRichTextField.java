package com.addcel.prosa.view.base.uicomponents.menu;


import com.addcel.prosa.view.base.UtilColor;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class OptionRichTextField extends RichTextField {

	private String option;
	private int width;
	
	public OptionRichTextField(String option) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		width = Display.getWidth();

		this.option = option;
	}


	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.LIST_DESCRIPTION_TITLE);
		graphics.drawText(option, 0, 0, DrawStyle.LEFT, width);

		super.paint(graphics);
	}
}

