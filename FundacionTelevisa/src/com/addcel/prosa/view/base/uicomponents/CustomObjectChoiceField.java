package com.addcel.prosa.view.base.uicomponents;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.ObjectChoiceField;

import com.addcel.prosa.view.base.UtilColor;

public class CustomObjectChoiceField extends ObjectChoiceField{

	public CustomObjectChoiceField(String label, Object[] choices, int initialIndex){
		super(label, choices, initialIndex);
	}
	
	
	protected void paint(Graphics graphics){
		
		graphics.setColor(UtilColor.ELEMENT_STRING);
		super.paint(graphics);
	}
}
