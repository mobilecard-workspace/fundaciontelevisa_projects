package com.addcel.api.dataaccess.components.connection;

import com.addcel.api.addcelexception.OwnException;



public interface Connectable {

	public void execute(String data) throws OwnException;
	public String createURL();
	public Object getData();
}
