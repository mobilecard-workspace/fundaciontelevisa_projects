package com.addcel.api.dataaccess.components.encryption;

public interface Encryptionable {

	public void execute(String data);
	public Object getData();
}
