package com.addcel.api.dataaccess;

import com.addcel.api.dataaccess.components.connection.Connectable;
import com.addcel.api.dataaccess.components.descryption.Descryptionable;
import com.addcel.api.dataaccess.components.encryption.Encryptionable;
import com.addcel.api.dataaccess.components.toJson.Jsonable;
import com.addcel.api.dataaccess.components.toObject.Objectable;
import com.addcel.prosa.view.base.Viewable;



public abstract class DataAccessible {
	
	public final static int ERROR = 0;
	public final static int DATA = 1;
	
	
	protected Viewable viewable;
	protected String data;
	
	protected Connectable connectable;
	protected Descryptionable descryptionable;
	protected Encryptionable encryptionable;
	protected Jsonable jsonable;
	protected Objectable objectable;
	
	public DataAccessible(Viewable viewable, String data){
		this.viewable = viewable;
		this.data = data;
	}
	
	abstract protected void execute(Viewable viewable, String json);
}
