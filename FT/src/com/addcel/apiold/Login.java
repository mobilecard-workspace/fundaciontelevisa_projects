package com.addcel.apiold;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.MainScreen;

import org.json.me.JSONObject;

import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.util.UtilBB;
import com.addcel.api.util.UtilSecurity;
import com.addcel.apiold.dto.UserBean;
import com.addcel.apiold.view.PasswordUpdater;
import com.addcel.apiold.view.SplashScreen;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.view.VSumateDatosAddcel;
import com.addcel.prosa.view.base.Viewable;

public class Login implements HttpListener {

	private String url = URL.URL_LOGIN;

	private String password = null;
	private String json = null;
	private String jsonEncrypt = null;
	private String jsonEncrypt2 = null;

	private String user = null;
	
	private String post;
	//private HttpPoster poster;
	private Viewable viewable;
	private MainScreen mainScreen;
	private SplashScreen splashScreen;


	public Login(Viewable viewable, String user, String password) {

		setClass(viewable, user, password);
	}

	
	public Login(Viewable viewable, String user, String password, MainScreen mainScreen) {

		this.mainScreen = mainScreen;
		setClass(viewable, user, password);
	}

	
	private void setClass(Viewable viewable, String user, String password){

		this.viewable = viewable;
		splashScreen = SplashScreen.getInstance();
		splashScreen.start();

		try {

			if ((user != null) && (password != null)) {

				this.user = user.trim();
				this.password = password.trim();
				json = createJSon(user, password);
				String parse = UtilSecurity.parsePass(password);
				jsonEncrypt = UtilSecurity.aesEncrypt(parse, json);
				jsonEncrypt2 = UtilSecurity.mergeStr(jsonEncrypt, password);
				this.post = "json=" + jsonEncrypt2;
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			viewable.setData(DataAccessible.ERROR, "Error de codificaci�n.");
		}
	}
	
	
	private String createJSon(String user, String password)
			throws UnsupportedEncodingException {

		StringBuffer json = new StringBuffer();

		this.user = user;
		
		json.append("{\"login\":\"").append(user);
		json.append("\",\"tipo\":\"").append(
				UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
		json.append("\",\"imei\":\"").append(UtilBB.getImei());
		json.append("\",\"modelo\":\"").append(
				UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
		json.append("\",\"software\":\"").append(
				UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
		json.append("\",\"key\":\"").append(UtilBB.getImei());
		json.append("\",\"password\":\"").append(password);
		json.append("\",\"passwordS\":\"").append(UtilSecurity.sha1(password));
		json.append("\"}");

		return json.toString();
	}

	public void connect() {

		String idealConnection = UtilBB.checkConnectionType();

		if (idealConnection != null) {

			if (this.url != null) {
				try {

					Communicator communicator = Communicator.getInstance();
					communicator.sendHttpRequest(this, this.url + "?" + this.post);

				} catch (Exception e) {
					getMessageError("No se conecto al servidor, intenta de nuevo por favor.");
				}
			}
		} else {
			//viewable.sendMessage("No se pudo crear una conexi�n, intenta de nuevo por favor.");
			getMessageError("No se pudo crear una conexi�n, intenta de nuevo por favor.");
		}
	}

	public void run() {

		connect();
	}

	public void handleHttpError(int errorCode, String error) {
		getMessageError(error);
	}

	public boolean isDestroyed() {
		return false;
	}

	public void receiveEstatus(String msg) {}

	public void receiveHeaders(Vector _headers) {}

	public void receiveHttpResponse(int appCode, byte[] response) {

		//splashScreen.remove();
		
		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				splashScreen.remove();
			}
		});
		
		
		String sTemp = null;
		
		try {
			sTemp = new String(response, 0, response.length, "UTF-8");
			
			sTemp = UtilSecurity.aesDecrypt(UtilSecurity.parsePass(password), sTemp);

			JSONParser jsParser = new JSONParser();

			if (jsParser.isLogin(sTemp)) {

				UserBean.nameLogin = user;
				
				if (viewable != null){
					UiApplication.getUiApplication().invokeLater(new Runnable() {
						public void run() {
							UiApplication.getUiApplication().popScreen((MainScreen) viewable);
							UiApplication.getUiApplication().pushScreen(new VSumateDatosAddcel(true, "Datos para donar", user, password));
						}
					});
				}

			} else if (jsParser.isLogin2(sTemp)) {

				UserBean.nameLogin = null;
				UserBean.idLogin = null;

				synchronized (Application.getEventLock()) {
					PasswordUpdater passwordUpdater = new PasswordUpdater(null);
					UiApplication.getUiApplication().pushScreen(passwordUpdater);
				}
				
			} else if (jsParser.isLogin3(sTemp)) {
				
				UserBean.nameLogin = null;
				UserBean.idLogin = null;
				
				getMessageError("Intente de nuevo por favor.");
				
			} else {

				UserBean.nameLogin = null;
				UserBean.idLogin = null;

				getMessageError("Error al capturar el usuario � contrase�a.");
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			getMessageError("Hubo un error al leer la informaci�n requerida.");

		} catch (Exception e) {
			e.printStackTrace();
			getMessageError("Hubo un error al procesar la informaci�n requerida.");
		}
	}

	private void setData(final Viewable viewable, final JSONObject jsObject){
		
		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				
				splashScreen.remove();
				
				viewable.setData(0, jsObject);
			}
		});
	}

	public void getMessageError(String error) {

		final String errors = error;
		
		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				splashScreen.remove();
				Dialog.alert(errors);
			}
		});
	}
}