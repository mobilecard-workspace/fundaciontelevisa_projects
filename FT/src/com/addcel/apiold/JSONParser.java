package com.addcel.apiold;



import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.apiold.dto.GeneralBean;
import com.addcel.apiold.dto.UserBean;

public class JSONParser {

	public JSONParser() {

	}

	public UserBean getUser(String json) {

		UserBean userBean = null;
		JSONObject jsObject = null;
		String user = "";
		String login = "";
		String pswd = "";
		String birthday = "";
		String phone = "";
		String registerDate = "";
		String name = "";
		String lastName = "";
		String address = "";
		String credit = "";
		String life = "";
		String bank = "";
		String creditType = "";
		String provider = "";
		String status = "";
		String mail = "";
		String idUser = "";
		String materno = "";
		String sexo = "";
		String tel_casa = "";
		String tel_oficina = "";
		String id_estado = "";
		String ciudad = "";
		String calle = "";
		String num_ext = "";
		String num_interior = "";
		String colonia = "";
		String cp = "";
		String dom_amex = "";
		
		try {

			jsObject = new JSONObject(json);

			if (jsObject.has("usuario")) {
				user = jsObject.getString("usuario");
			}

			if (jsObject.has("login")) {
				login = jsObject.getString("login");
			}

			if (jsObject.has("password")) {
				pswd = jsObject.getString("password");
			}

			if (jsObject.has("nacimiento")) {
				birthday = jsObject.getString("nacimiento");
			}

			if (jsObject.has("telefono")) {
				phone = jsObject.getString("telefono");
			}

			if (jsObject.has("registro")) {
				registerDate = jsObject.getString("registro");
			}

			if (jsObject.has("nombre")) {
				name = jsObject.getString("nombre");
			}

			if (jsObject.has("apellido")) {
				lastName = jsObject.getString("apellido");
			}

			if (jsObject.has("direccion")) {
				address = jsObject.getString("direccion");
			}

			if (jsObject.has("tarjeta")) {
				credit = jsObject.getString("tarjeta");
			}

			if (jsObject.has("vigencia")) {
				life = jsObject.getString("vigencia");
			}

			if (jsObject.has("banco")) {
				bank = jsObject.getString("banco");
			}

			if (jsObject.has("tipotarjeta")) {
				creditType = jsObject.getString("tipotarjeta");
			}

			if (jsObject.has("proveedor")) {
				provider = jsObject.getString("proveedor");
			}

			if (jsObject.has("status")) {
				status = jsObject.getString("status");
			}
			if (jsObject.has("mail")) {
				mail = jsObject.getString("mail");
			}
			if (jsObject.has("usuario")) {
				idUser = jsObject.getString("usuario");
			}
			if (jsObject.has("materno")) {
				materno = jsObject.getString("materno");
			}
			if (jsObject.has("sexo")) {
				sexo = jsObject.getString("sexo");
			}
			if (jsObject.has("tel_casa")) {
				tel_casa = jsObject.getString("tel_casa");
			}
			if (jsObject.has("tel_oficina")) {
				tel_oficina = jsObject.getString("tel_oficina");
			}
			if (jsObject.has("id_estado")) {
				id_estado = jsObject.getString("id_estado");
			}
			if (jsObject.has("ciudad")) {
				ciudad = jsObject.getString("ciudad");
			}
			if (jsObject.has("calle")) {
				calle = jsObject.getString("calle");
			}
			if (jsObject.has("num_ext")) {
				num_ext = jsObject.getString("num_ext");
			}
			if (jsObject.has("num_interior")) {
				num_interior = jsObject.getString("num_interior");
			}
			if (jsObject.has("colonia")) {
				colonia = jsObject.getString("colonia");
			}
			if (jsObject.has("cp")) {
				cp = jsObject.getString("cp");
			}
			if (jsObject.has("dom_amex")) {
				dom_amex = jsObject.getString("dom_amex");
			}
			/*
			 * String =""; String sexo=""; String tel_casa=""; String
			 * tel_oficina=""; String id_estado=""; String ciudad=""; String
			 * calle=""; String num_ext=""; String num_interior=""; String
			 * colonia=""; String cp=""; String dom_amex="";
			 */

			userBean = new UserBean(login, pswd, birthday, phone, registerDate,
					name, lastName, address, credit, life, bank, creditType,
					provider, status, mail, idUser, materno, sexo, tel_casa,
					tel_oficina, id_estado, ciudad, calle, num_ext,
					num_interior, colonia, cp, dom_amex);

		} catch (JSONException e) {
			e.printStackTrace();
			return userBean;
		}

		return userBean;

	}

	public boolean isLogin(String json) {

		boolean isLogin = false;
		String msj = "";
		String id = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("resultado")) {

				msj = jsObject.getString("resultado");

			}
			if (jsObject.has("mensaje")) {

				id = jsObject.getString("mensaje");
				// MainClass.IdUser=id;

			}

			if (msj.equals("1")) {
				if (id.indexOf("|") != -1) {
					String mensaje = id.substring(id.indexOf("|") + 1,
							id.length());
					UserBean.idLogin=mensaje;
				}

				isLogin = true;
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return isLogin;
		}

		return isLogin;

	}

	public boolean isLogin2(String json) {

		boolean isLogin = false;
		String msj = "";
		String id = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("resultado")) {
				msj = jsObject.getString("resultado");
			}
			if (jsObject.has("mensaje")) {
				id = jsObject.getString("mensaje");
			}

			if (msj.equals("99")) {

				if (id.indexOf("|") != -1) {
					String mensaje = id.substring(0, id.indexOf("|"));
					String iduser = id.substring(id.indexOf("|") + 1,
							id.length());
				}
				isLogin = true;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return isLogin;
	}

	
	public boolean isLogin3(String json) {

		boolean isLogin = false;
		String msj = "";
		String id = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("resultado")) {

				msj = jsObject.getString("resultado");

			}
			if (jsObject.has("mensaje")) {

				id = jsObject.getString("mensaje");
			}

			if (msj.equals("98")) {
				if (id.indexOf("|") != -1) {
					String mensaje = id.substring(0, id.indexOf("|"));
					String iduser = id.substring(id.indexOf("|") + 1,
							id.length());
					System.out.println("mensaje " + mensaje);
					System.out.println("iduser " + iduser);
				}
				isLogin = true;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return isLogin;

	}

	public String setSMS(String json) {

		String msj = "";
		String id = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("resultado")) {

				msj = jsObject.getString("resultado");

			}
			if (jsObject.has("mensaje")) {

				id = jsObject.getString("mensaje");
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return id;

	}

	public boolean setTag(String json) {

		boolean isLogin = false;
		String msj = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("resultado")) {

				msj = jsObject.getString("resultado");

			}

			if (msj.equals("0")) {
				isLogin = true;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return isLogin;

	}

	public String getMessageError(String json) {

		String msj = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("mensaje")) {

				msj = jsObject.getString("mensaje");

			}

		} catch (JSONException e) {
			e.printStackTrace();
			msj = "Favor de intentar m�s tarde.";
		}

		return msj;
	}

	
	public boolean isRegister(String json) {

		boolean isRegister = false;
		String msj = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("resultado")) {

				msj = jsObject.getString("resultado");

			}

			if (msj.equals("1")) {
				isRegister = true;
			}

		} catch (JSONException e) {
			e.printStackTrace();
			return isRegister;
		}

		return isRegister;

	}

	public String getMessage(String json) {

		String message = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("mensaje")) {

				message = jsObject.getString("mensaje");

			}

		} catch (JSONException e) {
			e.printStackTrace();
			message = "Favor de intentar m�s tarde.";
		}

		return message;
	}

	
	public GeneralBean[] getInfoCivil(String type, String json) {

		GeneralBean[] bankBeans = null;
		JSONObject jsObject = null;
		JSONObject jsObjectInternal = null;
		JSONArray jsArray = null;
		int lenght = 0;
		String description = "";
		String clave = "";

		try {

			jsObject = new JSONObject(json);

			jsArray = jsObject.getJSONArray(type);

			lenght = jsArray.length();

			bankBeans = new GeneralBean[lenght];

			for (int i = 0; i < lenght; i++) {

				jsObjectInternal = jsArray.getJSONObject(i);

				if (jsObjectInternal.has("descripcion")) {

					description = jsObjectInternal.getString("descripcion");

				}

				if (jsObjectInternal.has("clave")) {

					clave = jsObjectInternal.getString("clave");

				}

				bankBeans[i] = new GeneralBean(description, clave);

			}

		} catch (JSONException e) {
			e.printStackTrace();
			bankBeans = new GeneralBean[0];
		}

		return bankBeans;
	}

	public GeneralBean[] getInfoCredits(String type, String json) {

		GeneralBean[] bankBeans = null;
		JSONObject jsObject = null;
		JSONObject jsObjectInternal = null;
		JSONArray jsArray = null;
		int lenght = 0;
		String description = "";
		String clave = "";

		try {

			jsObject = new JSONObject(json);

			jsArray = jsObject.getJSONArray(type);

			lenght = jsArray.length();

			bankBeans = new GeneralBean[lenght];

			for (int i = 0; i < lenght; i++) {

				jsObjectInternal = jsArray.getJSONObject(i);

				if (jsObjectInternal.has("descripcion")) {

					description = jsObjectInternal.getString("descripcion");

				}

				if (jsObjectInternal.has("clave")) {

					clave = jsObjectInternal.getString("clave");

				}

				bankBeans[i] = new GeneralBean(description, clave);

			}

		} catch (JSONException e) {
			e.printStackTrace();
			bankBeans = new GeneralBean[0];
		}

		return bankBeans;
	}

	
	public GeneralBean[] getProducts(String type, String json) {
		System.out.println(json);
		GeneralBean[] bankBeans = null;
		JSONObject jsObject = null;
		JSONObject jsObjectInternal = null;
		JSONArray jsArray = null;
		int lenght = 0;
		String description = "";
		String clave = "";
		String claveE = "";
		String nombre = "no";

		try {

			jsObject = new JSONObject(json);

			jsArray = jsObject.getJSONArray(type);

			lenght = jsArray.length();

			bankBeans = new GeneralBean[lenght];

			for (int i = 0; i < lenght; i++) {

				jsObjectInternal = jsArray.getJSONObject(i);

				if (jsObjectInternal.has("descripcion")) {

					description = jsObjectInternal.getString("descripcion");

				}

				if (jsObjectInternal.has("claveWS")) {

					clave = jsObjectInternal.getString("claveWS");

				}
				if (jsObjectInternal.has("nombre")) {

					nombre = jsObjectInternal.getString("nombre");

				}

				if (jsObjectInternal.has("clave")) {

					claveE = jsObjectInternal.getString("clave");

				}

				bankBeans[i] = new GeneralBean(description, clave, nombre,
						claveE);

			}

		} catch (JSONException e) {
			e.printStackTrace();
			bankBeans = new GeneralBean[0];
		}

		return bankBeans;
	}
}
