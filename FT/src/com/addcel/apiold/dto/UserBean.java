package com.addcel.apiold.dto;

public class UserBean {
	
	private String login = "";
	private String pswd = "";
	private String birthday = "";
	private String phone = "";
	private String registerDate = "";
	private String name = "";
	private String lastName = "";
	private String address = "";
	private String credit = "";
	private String life = "";
	private String bank = "";
	private String creditType = "";
	private String provider = "";
	private String status = "";	
	private String mail = "";	
	private String idUsuario = "";
	
	public static String passTEMP = "xCL8m39sJ1";
	public static String nameLogin = null;
	public static String idLogin = null;
	
	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getTel_casa() {
		return tel_casa;
	}

	public void setTel_casa(String tel_casa) {
		this.tel_casa = tel_casa;
	}

	public String getTel_oficina() {
		return tel_oficina;
	}

	public void setTel_oficina(String tel_oficina) {
		this.tel_oficina = tel_oficina;
	}

	public String getId_estado() {
		return id_estado;
	}

	public void setId_estado(String id_estado) {
		this.id_estado = id_estado;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNum_ext() {
		return num_ext;
	}

	public void setNum_ext(String num_ext) {
		this.num_ext = num_ext;
	}

	public String getNum_interior() {
		return num_interior;
	}

	public void setNum_interior(String num_interior) {
		this.num_interior = num_interior;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getDom_amex() {
		return dom_amex;
	}

	public void setDom_amex(String dom_amex) {
		this.dom_amex = dom_amex;
	}

	private String materno="";
	private String sexo="";
	private String tel_casa="";
	private String tel_oficina="";
	private String id_estado="";
	private String ciudad="";
	private String calle="";
	private String num_ext="";
	private String num_interior="";
	private String colonia="";
	private String cp="";
	private String dom_amex="";
	
	public UserBean() {
		
	}
	
	public UserBean(String login, String pswd, String birthday, String phone,
	//public UserBean(String login, String pswd, String birthday, int phone,
			String registerDate, String name, String address, String credit,
			String life, String bank, String creditType, String provider,
			String status) {
		
		this.login = login;
		this.pswd = pswd;
		this.birthday = birthday;
		this.phone = phone;
		this.registerDate = registerDate;
		this.name = name;
		this.address = address;
		this.credit = credit;
		this.life = life;
		this.bank = bank;
		this.creditType = creditType;
		this.provider = provider;
		this.status = status;
	}
	
	
	
	public UserBean(String login, String pswd, String birthday, String phone,
	//public UserBean(String login, String pswd, String birthday, int phone,
			String registerDate, String name, String lastName, String address,
			String credit, String life, String bank, String creditType,
			String provider, String status,String Mail,String idUsuario,String materno,String sexo,String tel_casa,
			String tel_oficina,String estado,String ciudad,String calle,String num_ext,String num_interior,String colonia,
			String cp,String dom_amex) {
		super();
		this.login = login;
		this.pswd = pswd;
		this.birthday = birthday;
		this.phone = phone;
		this.registerDate = registerDate;
		this.name = name;
		this.lastName = lastName;
		this.address = address;
		this.credit = credit;
		this.life = life;
		this.bank = bank;
		this.creditType = creditType;
		this.provider = provider;
		this.status = status;
		this.mail=Mail;
		this.idUsuario=idUsuario;
		this.materno=materno;
		this.sexo=sexo;
		this.tel_casa=tel_casa;
		this.tel_oficina=tel_oficina;
		this.id_estado=estado;
		this.ciudad=ciudad;
		this.calle=calle;
		this.num_ext=num_ext;
		this.num_interior=num_interior;
		this.colonia=colonia;
		this.cp=cp;
		this.dom_amex=dom_amex;
	}

	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getIdUser() {
		return idUsuario;
	}
	
	public void setIdUser(String iduser) {
		this.idUsuario = iduser;
	}
	public String getMail() {
		return mail;
	}
	
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String getPswd() {
		return pswd;
	}
	
	public void setPswd(String pswd) {
		this.pswd = pswd;
	}
	
	public String getBirthday() {
		return birthday;
	}
	
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getRegisterDate() {
		return registerDate;
	}
	
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCredit() {
		return credit;
	}
	
	public void setCredit(String credit) {
		this.credit = credit;
	}
	
	public String getLife() {
		return life;
	}
	
	public void setLife(String life) {
		this.life = life;
	}
	
	public String getBank() {
		return bank;
	}
	
	public void setBank(String bank) {
		this.bank = bank;
	}
	
	public String getCreditType() {
		return creditType;
	}
	
	public void setCreditType(String creditType) {
		this.creditType = creditType;
	}
	
	public String getProvider() {
		return provider;
	}
	
	public void setProvider(String provider) {
		this.provider = provider;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
