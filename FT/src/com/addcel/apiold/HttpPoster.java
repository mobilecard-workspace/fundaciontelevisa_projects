package com.addcel.apiold;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import com.addcel.api.util.UtilBB;


public class HttpPoster implements Runnable {

	//private ContentConnection connection = null;
	private volatile boolean aborting = false;
	private Vector listenerQueue = new Vector();
	private boolean post = false;
	private String stringPost = null;
	private Vector URLQueue = new Vector();
	private int intentos = 1;
	private HttpConnection conn = null;
	private int flag = 0;

	private String url = "";
	private HttpListener listener = null;

	public HttpPoster() {
		Thread thread = new Thread(this);
		thread.start();
	}

	public synchronized void sendHttpRequest(HttpListener listener, String URL) {

		if (aborting == true) {
			aborting = false;
			this.run();
		}

		listenerQueue.addElement(listener);
		stringPost = null;
		post = false;
		URLQueue.addElement(URL);
		notify(); // wake up sending thread
	}

	public synchronized void sendHttpRequest(HttpListener listener, String URL,
			String _post) {

		if (aborting == true) {
			aborting = false;
			this.run();
		}

		stringPost = _post;
		post = true;
		listenerQueue.addElement(listener);

		URLQueue.addElement(URL);
		notify(); // wake up sending thread
	}

	public void run() {
		running: while (!aborting) {
			url = "";
			synchronized (this) {
				while (listenerQueue.size() == 0) {
					try {
						wait(); // releases lock

					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					if (aborting) {
						break running;
					}
				}

				listener = (HttpListener) (listenerQueue.elementAt(0));
				url = (String) (URLQueue.elementAt(0));

				listenerQueue.removeElementAt(0);
				URLQueue.removeElementAt(0);
			}
			if (post)
				doSend(listener, url, stringPost);
			else
				doSend(listener, url);
		}
	}

	private void doSend(HttpListener listener, String url, String post) {

		int code = 0;
		DataInputStream in = null;
		OutputStream dos;
		String errorStr = null;
		boolean wasError = false;
		ByteArrayOutputStream responseBytes = null;
		HttpConnection connection = null;
		byte data[] = null;
		byte postByte[] = null;
		String idealConnection = UtilBB.checkConnectionType();
		url = url + ";" + idealConnection + ";" + "ConnectionTimeout=30000";

		try {

			connection = (HttpConnection) Connector.open(url,
					Connector.READ_WRITE, true);

			postByte = new byte[post.length()];
			postByte = post.getBytes();
			connection.setRequestMethod(HttpConnection.POST);
			connection.setRequestProperty("content-length", postByte.length
					+ "");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			dos = connection.openOutputStream();
			dos.write(postByte);
			dos.flush();
			dos.close();
			// poner post
			in = connection.openDataInputStream();

			int length = (int) connection.getLength();

			if (length != -1) {
				data = new byte[length];

				// Read the png into an array
				in.readFully(data);
			} else {
				// Length not available...
				responseBytes = new ByteArrayOutputStream();
				int ch;
				while ((ch = in.read()) != -1)
					responseBytes.write(ch);

				data = responseBytes.toByteArray();
			}
		} catch (Exception e) {
			wasError = true;
			errorStr = "Error al conectarse al servidor.";
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (wasError) {
			if (intentos >= 1) {
				if (!aborting) {
					if (listener != null && !listener.isDestroyed())
						listener.handleHttpError(code, errorStr);
				}
			} else {
				System.gc();
			}
		} else {

			try {
				if (listener != null && !listener.isDestroyed()) {
					listener.receiveHttpResponse(0, data);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void doSend(HttpListener listener, String url) {
		int code = 0;
		DataInputStream in = null;
		String errorStr = null;
		boolean wasError = false;
		ByteArrayOutputStream responseBytes = null;
		HttpConnection connection = null;
		byte data[] = null;

		String idealConnection = UtilBB.checkConnectionType();

		url = url + ";" + idealConnection + ";" + "ConnectionTimeout=30000";

		try {
			connection = (HttpConnection) Connector.open(url,
					Connector.READ_WRITE, true);

			connection.setRequestMethod(HttpConnection.GET);
			connection.setRequestProperty("User-Agent",
					"Profile/MIDP-1.0 Confirguration/CLDC-1.0");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");

			getConnectionInformation(connection);

			in = connection.openDataInputStream();

			int length = (int) connection.getLength();

			if (length != -1) {
				data = new byte[length];
				in.readFully(data);
			} else {// Length not available...
				responseBytes = new ByteArrayOutputStream();
				int ch;
				while ((ch = in.read()) != -1) {
					responseBytes.write(ch);
				}
				data = responseBytes.toByteArray();
			}
		} catch (Exception e) {
			wasError = true;
			errorStr = "Error al conectarse al servidor.";
			e.printStackTrace();
		} finally {

			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				if (connection != null) {
					connection.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (wasError) {
			if (intentos >= 1) {
				if (!aborting) {
					if (listener != null && !listener.isDestroyed()) {
						listener.handleHttpError(code, errorStr);
					}
				}
			} else {
				System.gc();
			}
		} else {

			try {
				if (listener != null && !listener.isDestroyed()) {
					listener.receiveHttpResponse(0, data);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	// The instance is useless after abort has been called.
	public void abort() {
		listenerQueue.removeAllElements();
		URLQueue.removeAllElements();
		synchronized (this) {
			notify();
			// wake up our posting thread and kill it
		}
	}

	public boolean isAborting() {
		return aborting;
	}

	public int getIntentos() {
		return intentos;
	}

	public void setIntentos(int value) {
		intentos = value;
	}

	public void SetFlag(int f) {
		flag = f;
	}

	public int GetFlag() {
		return flag;
	}

	public void closeConnection() {

		try {

			if (conn != null) {
				conn.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void getConnectionInformation(HttpConnection hc) {

		// System.out.println("Request Method for this connection is " +
		// hc.getRequestMethod());
		// System.out.println("URL in this connection is " + hc.getURL());
		// System.out.println("Protocol for this connection is " +
		// hc.getProtocol()); // It better be HTTP:)
		// System.out.println("This object is connected to " + hc.getHost() +
		// " host");
		// System.out.println("HTTP Port in use is " + hc.getPort());
		// System.out.println("Query parameter in this request are " +
		// hc.getQuery());

	}
}
