package com.addcel.apiold;



public class Communicator {

	private static Communicator instance = null;
	private HttpPoster poster = null;

	private Communicator() {
		poster = new HttpPoster();
	}

	public synchronized static Communicator getInstance() {
		if (instance == null) {
			synchronized (Communicator.class) {
				Communicator inst = instance;
				if (inst == null) {
					synchronized (Communicator.class) {
						instance = new Communicator();
					}
				}
			}
		}
		return instance;
	}

	/**
	 * Metodo para iniciar una conexion con algun servidor
	 * 
	 * @param _listener
	 *            Objeto en el cual se va a depositar el resultado de la
	 *            peticion
	 * @param _url
	 *            Direccion a la cual se requiere la conexion
	 */
	public void sendHttpRequest(HttpListener _listener, String _url) {
		poster.sendHttpRequest(_listener, _url);

	}

	/**
	 * Metodo para iniciar una conexion con algun servidor
	 * 
	 * @param _listener
	 *            Objeto en el cual se va a depositar el resultado de la
	 *            peticion
	 * @param _url
	 *            Direccion a la cual se requiere la conexion
	 * @param _tries
	 *            Numero de intentos de conexion
	 */
	public void sendHttpRequest(HttpListener _listener, String _url, int _tries) {
		poster.setIntentos(_tries);
		poster.sendHttpRequest(_listener, _url);
	}

	
	public void sendHttpPost(HttpListener _listener, String _url, String _post) {

		poster.sendHttpRequest(_listener, _url, _post);
	}
	
	
	/**
	 * Destructor del objeto de comunicaciones, es necesario su llamada para
	 * detener el hilo y liberar la memoria
	 */
	public void destroy() {

		if (poster != null) {
			poster.abort();
			poster.closeConnection();
			poster = null;
		}
	}

	public void reestart() {

		if (poster != null) {
			poster.abort();
			poster.closeConnection();
			poster = new HttpPoster();
		}
	}

	/** Metodo para abortar una conexion */
	public void abort() {
		if (poster != null) {
			poster.closeConnection();
		}
	}



}
