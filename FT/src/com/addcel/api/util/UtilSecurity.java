package com.addcel.api.util;

import java.io.UnsupportedEncodingException;

import com.addcel.api.util.security.AESBinFormat;


import net.rim.device.api.crypto.SHA1Digest;

public class UtilSecurity {

	public static String key = "1234567890ABCDEF0123456789ABCDEF";
	public static String passTEMP = "xCL8m39sJ1";
	public static String TAG_REGISTER = "\"etiqueta\":\""+""+"\",\"numero\":\""+"0"+"\",\"dv\":\""+"0"+"\",\"idtiporecargatag\":\""+"1";
	
/*

		try {
			outputLen += cipher.doFinal(outputBytes, outputLen);
		} catch (InvalidCipherTextException e) {
			throw new IOException("Can't encrypt file: "
					+ e.getMessage());
		}
*/

	public static String aesEncrypt(String seed, String cleartext) {
		String encryptedText;
		
		try{
			
			String text= AESBinFormat.encode(UtilSecurity.replaceConAcento(cleartext), seed);
			return text;
		}catch(Exception e){
			encryptedText = "";
		}
		 	   
		return encryptedText;
	}
	
	
	public static String aesDecrypt(String seed, String encrypted) {
		String decryptedText;
		
		try{
			return UtilSecurity.reemplazaHTML(AESBinFormat.decode(encrypted, seed));
		}catch(Exception e){
			decryptedText = "";
		}
		
		return decryptedText;
	}
	

	public static String parsePass(String pass) {
		int len = pass.length();
		String key = "";

		for (int i = 0; i < 32 / len; i++) {
			key += pass;
		}

		int carry = 0;
		while (key.length() < 32) {
			key += pass.charAt(carry);
			carry++;
		}
		return key;
	}

	
	public static String reemplazaHTML(String html) {
		String caracteres[] = { "&Aacute;", "&aacute;", "&Eacute;", "&eacute;",
				"&Iacute;", "&iacute;", "&Oacute;", "&oacute;", "&Uacute;",
				"&uacute;", "&Ntilde;", "&ntilde;" };
		String letras[] = { "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",
				"�", "�" };

		for (int counter = 0; counter < caracteres.length; counter++) {
			html = reemplaza(html, caracteres[counter], letras[counter]);
		}
		return html;
	}
	
	public static String reemplaza(String src, String orig, String nuevo) {
		if (src == null) {
			return null;
		}
		int tmp = 0;
		String srcnew = "";
		while (tmp >= 0) {
			tmp = src.indexOf(orig);
			if (tmp >= 0) {
				if (tmp > 0) {
					srcnew += src.substring(0, tmp);
				}
				srcnew += nuevo;
				src = src.substring(tmp + orig.length());
			}
		}
		srcnew += src;
		return srcnew;
	}
	
	public static String replaceConAcento(String text) {
		StringBuffer sBuffer = new StringBuffer();
		for (int i = 0; i < text.length(); i++) {
			if (text.charAt(i) == '�')
				sBuffer.append("&Ntilde;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&ntilde;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Aacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&aacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Eacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&eacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Iacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&iacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Oacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&oacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Uacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&uacute;");
			else
				sBuffer.append(text.charAt(i));
		}

		return sBuffer.toString();
	}
	
	
	
	public static String sha1(String text) throws UnsupportedEncodingException {

		SHA1Digest sha1Digest = new SHA1Digest();
		String sha = text;
		byte[] inpData = sha.getBytes("UTF-8");
		sha1Digest.update(inpData, 0, inpData.length);
		byte[] digest = sha1Digest.getDigest();
		StringBuffer shaRes = new StringBuffer(40); // 40 hex char is size of
													// 160-bit SHA-1 result
		for (int a = 0; a < digest.length; a++) {
			String tmp = Integer.toHexString(0xff & digest[a]);
			if (tmp.length() == 1) // If hex value is "0X" then tmp is just one
									// digit "X"
			{
				shaRes.append('0');
			}
			shaRes.append(tmp);
		}

		return shaRes.toString();
	}
	

	public static String mergeStr(String first, String second) {

		String result = "";
		String other = UtilSecurity.reverse(second);

		result += (Integer.toString(other.length()).length() < 2) ? "0"
				+ other.length() : Integer.toString(other.length());

		String sub1 = first.substring(0, 19);
		String sub2 = first.substring(19, first.length());

		result += sub1;

		for (int i = 0; i < other.length(); i += 2) {
			int offset = ((i + 2) <= other.length()) ? (i + 2) : (i + 1);

			String next = other.substring(i, offset);

			result += next;

			result += sub2.substring(0, 2);

			sub2 = sub2.substring(2);

		}
		result += sub2;

		return result;
	}


	public static String reverse(String chain) {
		String nuevo = "";

		for (int i = chain.length() - 1; i >= 0; i--) {
			nuevo += chain.charAt(i);
		}

		return nuevo;
	}

}
