package com.addcel.api.addcelexception;

public class Error {

	public static String HTTP_SEND_DATA = "No hay conexión con el servicio.";
	public static String HTTP_RECEIVE_DATA = "No se recibio información del servicio.";
	public static String JSON_EXCEPTION = "No se pudo interpretar la contestación del servidor.";
	
	public static String NULLPOINTER_EXCEPTION = "No se obtuvo la información.";
}
