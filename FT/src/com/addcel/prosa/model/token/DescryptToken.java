package com.addcel.prosa.model.token;

import com.addcel.api.dataaccess.components.descryption.Descryptionable;
import com.addcel.api.util.security.AddcelCrypto;


public class DescryptToken implements Descryptionable{

	private String data;
	
	public void execute(String data) {

		this.data = AddcelCrypto.decryptHard(data);
	}

	public Object getData() {

		return data;
	}



}
