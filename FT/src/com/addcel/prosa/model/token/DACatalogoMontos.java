package com.addcel.prosa.model.token;

import java.util.Vector;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.connection.http.MethodGET;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.view.base.Viewable;


public class DACatalogoMontos extends DataAccessible implements Runnable {

	public DACatalogoMontos(Viewable viewable, String data) {
		super(viewable, data);
	}

	public void run() {
		execute(viewable, data);
	}

	public void execute(Viewable viewable, String json) {

		try {

			this.jsonable = null;
			this.objectable = null;
			
			this.encryptionable = new EncryptToken();
			encryptionable.execute(json);
			String encrypt = (String) encryptionable.getData();
			
			connectable = new MethodGET(URL.URL_GET_CATALOGO_MONTOS);
			connectable.execute(encrypt);
			
			String descrypt = (String)connectable.getData();
			

			this.descryptionable = new DescryptToken();
			
			descryptionable.execute(descrypt);
			
			descrypt = (String)descryptionable.getData();
			
			OCatalogoMontos catalogoMontos = new OCatalogoMontos();
			catalogoMontos.execute(descrypt);
			
			Vector montos = (Vector)catalogoMontos.getData();

			viewable.setData(DataAccessible.DATA, montos);
			
		} catch (OwnException e) {
			viewable.setData(DataAccessible.ERROR, e.toString());
		}
	}
}

/*
this.encryptionable = new EncryptToken();
encryptionable.execute(json);
String encrypt = (String) encryptionable.getData();

connectable = new MethodGET(URL.URL_GET_TOKEN);
connectable.execute(encrypt);

String descrypt = (String)connectable.getData();


this.descryptionable = new DescryptToken();

descryptionable.execute(descrypt);

descrypt = (String)descryptionable.getData();


this.objectable = new OToken();
objectable.execute(descrypt);
Token token = (Token)objectable.getData();

viewable.setData(DataAccessible.DATA, token);
*/
