package com.addcel.prosa.model.token;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.toObject.Objectable;
import com.addcel.prosa.dto.Token;

public class OToken implements Objectable{

	private Token token;
	
	public void execute(String data) throws OwnException {

		try {
			JSONObject jsonObject = new JSONObject(data);
			
			token = new Token();
			
			token.setToken(jsonObject.optString("token"));
			token.setIdError(jsonObject.optInt("idError"));
			token.setMensajeError(jsonObject.optString("mensajeError"));
			
		} catch (JSONException e) {
			e.printStackTrace();
			throw new OwnException(Error.JSON_EXCEPTION);
		}
	}

	public Object getData() {

		return token;
	}

}
