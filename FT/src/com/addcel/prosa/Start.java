package com.addcel.prosa;

import net.rim.device.api.ui.UiApplication;

import com.addcel.api.util.UtilBB;
import com.addcel.prosa.view.VSplash;

public class Start extends UiApplication
{
	
	static public String IDEAL_CONNECTION = null;
	static public String ID_PROVEEDOR = "22";

    public static void main(String[] args){
    	Start theApp = new Start();       
        theApp.enterEventDispatcher();
    }
    

    public Start(){        
    	IDEAL_CONNECTION = UtilBB.checkConnectionType();
        pushScreen(new VSplash());
    }    
}
