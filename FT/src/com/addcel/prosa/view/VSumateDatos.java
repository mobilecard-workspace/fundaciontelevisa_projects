package com.addcel.prosa.view;

import java.util.Vector;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.component.RadioButtonGroup;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.prosa.Start;
import com.addcel.prosa.dto.Monto;
import com.addcel.prosa.dto.PagoRespuesta;
import com.addcel.prosa.model.token.DACatalogoMontos;
import com.addcel.prosa.model.token.DAPagoTarjeta;
import com.addcel.prosa.view.base.UtilDate;
import com.addcel.prosa.view.base.Viewable;
import com.addcel.prosa.view.base.uicomponents.CustomEditField;
import com.addcel.prosa.view.base.uicomponents.CustomObjectChoiceField;
import com.addcel.prosa.view.base.uicomponents.CustomRadioButtonField;
import com.addcel.prosa.view.base.uicomponents.CustomSelectedSizeButton;
import com.addcel.prosa.view.base.uicomponents.ElementLabelField;


public class VSumateDatos extends Viewable implements FieldChangeListener{

	private ElementLabelField emailTxt;
	private ElementLabelField nombreTxt;
	
	private ElementLabelField montoTxt;
	
	private ElementLabelField numeroTarjetaTxt;
	private ElementLabelField cvv2Txt;
	private ElementLabelField vigenciaTxt;
	private ElementLabelField tipoTarjetaTxt;	

	private CustomEditField emailEdit;
	private CustomEditField nombreEdit;
	
	private CustomObjectChoiceField montoChoice;
	
	private CustomEditField montoEdit;
	private CustomEditField numeroTarjetaEdit;
	private CustomEditField cvv2Edit;

	private CustomObjectChoiceField anio;
	private CustomObjectChoiceField mes;

	private CustomRadioButtonField rbVisa = null;
	private CustomRadioButtonField rbMaster = null;
	
	private CustomSelectedSizeButton donar;
	
	private int tarjetaCredito = 0;
	
	public VSumateDatos(boolean isSetTitle, String title) {
		super(isSetTitle, title);

		emailTxt = new ElementLabelField("Email:");
		nombreTxt = new ElementLabelField("Nombre:");

		montoTxt = new ElementLabelField("Otro monto:");

		numeroTarjetaTxt = new ElementLabelField("N�mero de tarjeta");
		cvv2Txt = new ElementLabelField("Cvv2");
		vigenciaTxt = new ElementLabelField("Vigencia");
		tipoTarjetaTxt = new ElementLabelField("Tipo de tarjeta");
				
		emailEdit = new CustomEditField(100, EditField.FILTER_EMAIL);
		nombreEdit = new CustomEditField(100, EditField.FILTER_DEFAULT);

		montoChoice = new CustomObjectChoiceField("Monto:", null, 0);
		montoChoice.setChangeListener(this);
		
		montoEdit = new CustomEditField(100, EditField.FILTER_INTEGER);
		
		numeroTarjetaEdit = new CustomEditField(16, EditField.FILTER_INTEGER);
		cvv2Edit = new CustomEditField(4, EditField.FILTER_INTEGER);
		
		anio = new CustomObjectChoiceField("A�o", UtilDate.getYears(7), 2);
		mes = new CustomObjectChoiceField("Mes", UtilDate.getNumberMonths(), 5);
		
        RadioButtonGroup rbg = new RadioButtonGroup();

        rbVisa = new CustomRadioButtonField("Visa",rbg,false);
        rbMaster = new CustomRadioButtonField("Mastercard",rbg,false);
        
		donar = new CustomSelectedSizeButton("Donar", 1);
		donar.setChangeListener(this);

		add(emailTxt);
		add(emailEdit);
		add(new LabelField());
		add(nombreTxt);
		add(nombreEdit);
		add(new LabelField());
		add(montoChoice);
		add(new LabelField());
		add(montoTxt);
		add(montoEdit);
		add(new LabelField());
		add(numeroTarjetaTxt);
		add(numeroTarjetaEdit);
		add(new LabelField());
		add(cvv2Txt);
		add(cvv2Edit);
		add(new LabelField());
		add(vigenciaTxt);
		add(mes);
		add(anio);
		add(new LabelField());
		add(tipoTarjetaTxt);
		add(rbVisa);
        add(rbMaster);
		add(new LabelField());
		add(donar);
		
		Thread thread = new Thread(new DACatalogoMontos(this, "{\"proveedor\":\"19\"}"));
		thread.start();
	}

	
	public void fieldChanged(Field arg0, int arg1) {

		if (arg0 == montoChoice){
			
			if (ObjectChoiceField.CONTEXT_CHANGE_OPTION == arg1){
				
				montoEdit.setText("");
			}

		} else if(arg0 == donar){
			
			if (rbVisa.isSelected()){
				tarjetaCredito = 1;
			} else if (rbMaster.isSelected()){
				tarjetaCredito = 2;
			} else {
				tarjetaCredito = 0;
			}
			
			
			if (tarjetaCredito > 0){
				
				
				if (checarCampos()){
					

					String sAnio = getInfo(anio).substring(2);
					String vigencia = getInfo(mes) + "/" + sAnio;
					
					
					JSONObject jsonObject = new JSONObject();
					JSONObject jsonObject1 = new JSONObject();

					try {
						
						jsonObject.put("email", emailEdit.getText());
						jsonObject.put("nombre", nombreEdit.getText());
						jsonObject.put("tipoTarjeta", String.valueOf(tarjetaCredito));
						jsonObject.put("tarjeta", numeroTarjetaEdit.getText());
						jsonObject.put("cvv2", cvv2Edit.getText());
						jsonObject.put("vigencia", vigencia);
						jsonObject.put("idProveedor", Start.ID_PROVEEDOR);						
						
						
						String monto = null;
						
						if (!invalidText(montoEdit)){
							
							monto = montoEdit.getText();
							jsonObject.put("monto", String.valueOf(montoEdit.getText()));
							
						} else {
							
							int index = montoChoice.getSelectedIndex();
							Monto object = (Monto)montoChoice.getChoice(index);
							
							monto = String.valueOf(object.getMonto());
							
							jsonObject.put("idMonto", object.getIdMonto());
							jsonObject.put("monto", monto);
							
						}
						
						jsonObject1.put("idProveedor", "19");
						jsonObject1.put("usuario", "userPrueba");
						jsonObject1.put("password", "passwordPrueba");
						
						DAPagoTarjeta pagoTarjeta = new DAPagoTarjeta(this, jsonObject1.toString(), monto);
						pagoTarjeta.setJson(jsonObject);
						Thread thread = new Thread(pagoTarjeta);
						thread.start();
						
						
					} catch (JSONException e) {
						this.analyzeData(DataAccessible.ERROR, Error.JSON_EXCEPTION);
						e.printStackTrace();
					}
					

				} else {
					this.analyzeData(DataAccessible.ERROR, "Existen campos sin informaci�n.");
				}
			} else {
				
				this.analyzeData(DataAccessible.ERROR, "No ha seleccionado un tipo de tarjeta.");
			}
		}
	}

	protected void analyzeData(int request, Object object) {
		
		
		if (request == DataAccessible.DATA){
			
			if (object instanceof Vector){
				
				Vector montos = (Vector)object;
				int size = montos.size();
				Monto catmontos[] = new Monto[size];
				montos.copyInto(catmontos);
				montoChoice.setChoices(catmontos);
			} else if(object instanceof PagoRespuesta){
				
				UiApplication.getUiApplication().pushScreen(new VSumateResultado(true, "", (PagoRespuesta)object));
			}

		} else if (request == DataAccessible.ERROR){
			
			Dialog.alert((String)object);
		}
	}
	
	
	
	private boolean checarCampos(){
		
		boolean check = true;
		
		if (invalidText(emailEdit)){
			check = false;
		} else if (invalidText(nombreEdit)){
			check = false;
		} else if (invalidText(numeroTarjetaEdit)){
			check = false;
		} else if (invalidText(cvv2Edit)){
			check = false;
		}
		
		return check;
	}
	

	private boolean invalidText(CustomEditField editField){
		
		String temp = editField.getText();
		
		if ((temp != null)&&(temp.length()>0)){
			return false;
		} else {
			return true;
		}
	}
	
	
	
	private String getInfo(ObjectChoiceField choiceField){
		
		String temp = null;
		
		int index = choiceField.getSelectedIndex();
		
		Object object = choiceField.getChoice(index);
		
		if (object instanceof String){
			
			temp = (String)object;
		} else {
			
			Monto monto = (Monto)object;
			temp = String.valueOf(monto.getMonto());
		}
		
		return temp;
	}
}
