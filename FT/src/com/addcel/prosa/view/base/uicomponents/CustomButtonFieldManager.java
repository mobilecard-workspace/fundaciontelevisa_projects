package com.addcel.prosa.view.base.uicomponents;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class CustomButtonFieldManager extends HorizontalFieldManager{

	private CustomSelectedButtonField one;
	private CustomSelectedButtonField two;
	private int width;
	private int preferredWidth;

	public CustomSelectedButtonField getOne(){
		return one;
	}
	
	public CustomSelectedButtonField getTwo(){
		return two;
	}
	
	public CustomButtonFieldManager(CustomSelectedButtonField one, CustomSelectedButtonField two){
		
		super();
		width = Display.getWidth();
		preferredWidth = width/2;
		this.one = one;
		this.two = two;
	}
	
	public int getPreferredWidth() {
		return preferredWidth;
	}
}
