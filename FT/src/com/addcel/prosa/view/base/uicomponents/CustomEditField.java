package com.addcel.prosa.view.base.uicomponents;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.addcel.prosa.view.base.UtilColor;

public class CustomEditField extends EditField{

	
	private Background focus;
	private Background unFocus;
	
	public CustomEditField(int maxNumChars, long style){
		
		super("", "", maxNumChars, style); 
		
		
		focus = BackgroundFactory.createSolidBackground(UtilColor.EDIT_TEXT_BACKGROUND_FOCUS);
		unFocus = BackgroundFactory.createSolidBackground(UtilColor.EDIT_TEXT_BACKGROUND_UNFOCUS);
		setBackground(unFocus);
	}
	
	
	protected void paint(Graphics graphics) {

		if (isFocus()){
			graphics.setColor(UtilColor.EDIT_TEXT_DATA_FOCUS);
		} else {
			graphics.setColor(UtilColor.EDIT_TEXT_DATA_UNFOCUS);
		}
		
		super.paint(graphics);
	}
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		setBackground(focus);
		invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		setBackground(unFocus);
		invalidate();
	}
	
}
