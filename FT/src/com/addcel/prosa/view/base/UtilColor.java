package com.addcel.prosa.view.base;

import net.rim.device.api.ui.Color;

public class UtilColor {
	
	private final static int BLANCO = Color.WHITE;
	private final static int GRIS = 0x636463;
	private final static int AMARILLO = 0xf6b454;
	private final static int NARANJA = 0xeb7a41;
	private final static int AZUL = 0x1e4367;
	
	
	public final static int MAIN_BACKGROUND = AZUL;

	public final static int TITLE_STRING = BLANCO;
	public final static int TITLE_BACKGROUND = NARANJA;

	public final static int BUTTON_FOCUS = AMARILLO;
	public final static int BUTTON_SELECTED = NARANJA;
	public final static int BUTTON_UNSELECTED = NARANJA;

	public final static int BUTTON_STRING_FOCUS = BLANCO;
	public final static int BUTTON_STRING_SELECTED = BLANCO;
	public final static int BUTTON_STRING_UNSELECTED = BLANCO;

	public final static int SUBTITLE_STRING = BLANCO;
	public final static int SUBTITLE_BACKGROUND = NARANJA;

	
	public final static int ELEMENT_STRING_CHOICE = BLANCO;
	public final static int ELEMENT_STRING = AMARILLO;
	public final static int ELEMENT_BACKGROUND = AZUL;

	public final static int EDIT_TEXT_DATA_FOCUS = GRIS;
	public final static int EDIT_TEXT_DATA_UNFOCUS = GRIS;
	
	public final static int EDIT_TEXT_BACKGROUND_FOCUS = BLANCO;
	public final static int EDIT_TEXT_BACKGROUND_UNFOCUS = BLANCO;

	public final static int LIST_DESCRIPTION_TITLE = NARANJA;
	public final static int LIST_DESCRIPTION_DATA = AMARILLO;

	public final static int LIST_BACKGROUND_SELECTED = BLANCO;
	public final static int LIST_BACKGROUND_UNSELECTED = AMARILLO;

}
