package com.addcel.prosa.view;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;

import com.addcel.apiold.Login;
import com.addcel.prosa.view.base.Viewable;
import com.addcel.prosa.view.base.uicomponents.CustomEditField;
import com.addcel.prosa.view.base.uicomponents.CustomPasswordEditField;
import com.addcel.prosa.view.base.uicomponents.CustomSelectedSizeButton;
import com.addcel.prosa.view.base.uicomponents.ElementLabelField;

public class VLogin extends Viewable implements FieldChangeListener{

	private ElementLabelField userTxt;
	private ElementLabelField passwordTxt;
	
	private CustomEditField userEdit;
	private CustomPasswordEditField passwordEdit;
	
	private CustomSelectedSizeButton iniciarSesion;
	private CustomSelectedSizeButton donar;
	
	public VLogin(boolean isSetTitle, String title) {
		super(isSetTitle, title);
		
		userTxt = new ElementLabelField("Usuario");
		passwordTxt = new ElementLabelField("Contrase�a");
		
		userEdit = new CustomEditField(16, EditField.CONSUME_INPUT);
		passwordEdit = new CustomPasswordEditField(12, EditField.CONSUME_INPUT);
		
		iniciarSesion = new CustomSelectedSizeButton("Iniciar sesi�n", 1);
		iniciarSesion.setChangeListener(this);
		
		donar = new CustomSelectedSizeButton("Donar", 1);
		donar.setChangeListener(this);
		
		add(userTxt);
		add(userEdit);
		
		add(passwordTxt);
		add(passwordEdit);
		add(new LabelField());
		add(iniciarSesion);
		add(donar);
	}

	public void fieldChanged(Field field, int context) {
		
		
		if (field == iniciarSesion){
			
			Login loginThread = new Login(this, userEdit.getText(), passwordEdit.getText());
			loginThread.run();
			
		} else if(field == donar){
			UiApplication.getUiApplication().pushScreen(new VSumateDatos(true, "Datos para donar"));
		}
		
	}

	protected void analyzeData(int request, Object object) {

	}

}
