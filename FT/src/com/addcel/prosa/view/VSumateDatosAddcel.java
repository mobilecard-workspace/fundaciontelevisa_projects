package com.addcel.prosa.view;

import java.util.Vector;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.prosa.Start;
import com.addcel.prosa.dto.Monto;
import com.addcel.prosa.dto.PagoRespuesta;
import com.addcel.prosa.model.token.DACatalogoMontos;
import com.addcel.prosa.model.token.DAPagoTarjetaAddcel;
import com.addcel.prosa.view.base.Viewable;
import com.addcel.prosa.view.base.uicomponents.CustomEditField;
import com.addcel.prosa.view.base.uicomponents.CustomObjectChoiceField;
import com.addcel.prosa.view.base.uicomponents.CustomPasswordEditField;
import com.addcel.prosa.view.base.uicomponents.CustomSelectedSizeButton;
import com.addcel.prosa.view.base.uicomponents.ElementLabelField;

public class VSumateDatosAddcel extends Viewable implements FieldChangeListener{

	private ElementLabelField montoTxt;
	private ElementLabelField cvv2Txt;
	private ElementLabelField passwordTxt;
	
	private CustomObjectChoiceField montoChoice;
	
	private CustomEditField montoEdit;
	private CustomEditField cvv2Edit;
	private CustomPasswordEditField passwordEdit;
	
	private CustomSelectedSizeButton donar;
	
	private int tarjetaCredito = 0;
	private String usuario;
	private String password;
	
	public VSumateDatosAddcel(boolean isSetTitle, String title, String usuario, String password) {
		super(isSetTitle, title);

		this.usuario = usuario;
		this.password = password;
		
		montoTxt = new ElementLabelField("Otro monto:");
		cvv2Txt = new ElementLabelField("Cvv2");
		passwordTxt = new ElementLabelField("Contraseña");
		montoChoice = new CustomObjectChoiceField("Monto:", null, 0);
		montoChoice.setChangeListener(this);
		montoEdit = new CustomEditField(100, EditField.FILTER_INTEGER);
		cvv2Edit = new CustomEditField(4, EditField.FILTER_INTEGER);
		passwordEdit = new CustomPasswordEditField(12, EditField.CONSUME_INPUT);
		donar = new CustomSelectedSizeButton("Donar", 1);
		donar.setChangeListener(this);

		add(new LabelField());
		add(montoChoice);
		add(new LabelField());
		add(montoTxt);
		add(montoEdit);
		add(new LabelField());
		add(cvv2Txt);
		add(cvv2Edit);
		add(new LabelField());
		add(passwordTxt);
		add(passwordEdit);
		add(new LabelField());
		add(donar);
		
		Thread thread = new Thread(new DACatalogoMontos(this, "{\"proveedor\":\"19\"}"));
		thread.start();
	}

	
	public void fieldChanged(Field arg0, int arg1) {

		if (arg0 == montoChoice){
			
			if (ObjectChoiceField.CONTEXT_CHANGE_OPTION == arg1){
				
				montoEdit.setText("");
			}

		} else if (arg0 == donar) {

			if (checarCampos()) {

				JSONObject jsonObject = new JSONObject();
				JSONObject jsonObject1 = new JSONObject();

				try {

					jsonObject.put("usuario", usuario);
					jsonObject.put("passwordMC", password);

					jsonObject.put("tipoTarjeta", String.valueOf(tarjetaCredito));
					jsonObject.put("cvv2", cvv2Edit.getText());
					jsonObject.put("idProveedor", Start.ID_PROVEEDOR);

					String monto = null;

					if (!invalidText(montoEdit)) {

						monto = montoEdit.getText();
						jsonObject.put("monto", String.valueOf(montoEdit.getText()));

					} else {

						int index = montoChoice.getSelectedIndex();
						Monto object = (Monto) montoChoice.getChoice(index);

						monto = String.valueOf(object.getMonto());

						jsonObject.put("idMonto", object.getIdMonto());
						jsonObject.put("monto", monto);

					}

					jsonObject1.put("idProveedor", "19");
					jsonObject1.put("usuario", "userPrueba");
					jsonObject1.put("password", "passwordPrueba");

					DAPagoTarjetaAddcel pagoTarjeta = new DAPagoTarjetaAddcel(this, jsonObject1.toString(), monto);
					pagoTarjeta.setJson(jsonObject);
					Thread thread = new Thread(pagoTarjeta);
					thread.start();

				} catch (JSONException e) {
					this.analyzeData(DataAccessible.ERROR, Error.JSON_EXCEPTION);
					e.printStackTrace();
				}

			} else {
				this.analyzeData(DataAccessible.ERROR, "Existen campos sin información.");
			}
		}
	}

	protected void analyzeData(int request, Object object) {
		
		if (request == DataAccessible.DATA){
			
			if (object instanceof Vector){
				
				Vector montos = (Vector)object;
				int size = montos.size();
				Monto catmontos[] = new Monto[size];
				montos.copyInto(catmontos);
				montoChoice.setChoices(catmontos);
			} else if(object instanceof PagoRespuesta){
				
				UiApplication.getUiApplication().pushScreen(new VSumateResultado(true, "", (PagoRespuesta)object));
			}
			
		} else if (object instanceof PagoRespuesta){
			
			PagoRespuesta pagoRespuesta = (PagoRespuesta)object;
			
			if (pagoRespuesta.getIdError() > 0){
				
				Dialog.alert(pagoRespuesta.getMensajeError());
			}
			
			
		} else if (request == DataAccessible.ERROR){
			
			Dialog.alert((String)object);
		}
	}
	

	private boolean checarCampos(){
		
		boolean check = true;
		
		if (invalidText(cvv2Edit)){
			check = false;
		}
		
		return check;
	}
	

	private boolean invalidText(CustomEditField editField){
		
		String temp = editField.getText();
		
		if ((temp != null)&&(temp.length()>0)){
			return false;
		} else {
			return true;
		}
	}
}
