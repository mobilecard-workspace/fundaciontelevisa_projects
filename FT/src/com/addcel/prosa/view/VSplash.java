package com.addcel.prosa.view;

import java.util.Timer;
import java.util.TimerTask;

import com.addcel.prosa.view.base.UtilIcon;
import com.addcel.prosa.view.base.Viewable;


import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;

public class VSplash extends Viewable {

	public VSplash() {

		super(false, null);

		BitmapField splash = UtilIcon.getSplash();

		add(splash);
		
		Timer timer = new Timer();

		Wait wait = new Wait(this);
		
		timer.schedule(wait, 4000);
	}

	class Wait extends TimerTask {

		private VSplash viewSplash;

		public Wait(VSplash viewSplash) {

			this.viewSplash = viewSplash;
		}

		public void run() {

			UiApplication.getUiApplication().invokeAndWait(new Runnable() {

				public void run() {
					UiApplication.getUiApplication().popScreen(viewSplash);
					UiApplication.getUiApplication().pushScreen(new VLogin(true, "Acceso"));
					//UiApplication.getUiApplication().pushScreen(new VSumateDatos(true, "Datos para donar"));
				}
			});
		}
	}

	protected void analyzeData(int request, Object object) {
		// TODO Auto-generated method stub
		
	}
}
